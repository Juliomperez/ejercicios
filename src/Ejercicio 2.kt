fun main() {
    var number1: Double
    var number2: Double
    var number3: Double
    var number4: Double
    var number5: Double

    println("Insert the first number: ")
    number1 = readLine()!!.toDouble()
    println("Insert the second number: ")
    number2 = readLine()!!.toDouble()
    println("Insert the third number: ")
    number3 = readLine()!!.toDouble()
    println("Insert the fourth number: ")
    number4 = readLine()!!.toDouble()
    println("Insert the fifth numbre: ")
    number5 = readLine()!!.toDouble()

    println("The numbers are: " + number1 + ", " + number2 + ", " + number3 + ", " + number4 + ", " + number5)

    var menor: Double = 0.0
    var mayor: Double = 0.0

    if (number1 < number2 && number1 < number3 && number1 < number4 && number1 < number5) {
        menor = number1
    } else if (number2 < number1 && number2 < number3 && number2 < number4 && number2 < number5) {
        menor = number2
    } else if (number3 < number1 && number3 < number2 && number3 < number4 && number3 < number5) {
        menor = number3
    } else if (number4 < number1 && number4 < number2 && number4 < number3 && number4 < number5) {
        menor = number4
    } else if (number5 < number1 && number5 < number2 && number5 < number3 && number5 < number4) {
        menor = number5
    }


    if (number1 > number2 && number1 > number3 && number1 > number4 && number1 > number5) {
        mayor = number1
    } else if (number2 > number1 && number2 > number3 && number2 > number4 && number2 > number5) {
        mayor = number2
    } else if (number3 > number1 && number3 > number2 && number3 > number4 && number3 > number5) {
        mayor = number3
    } else if (number4 > number1 && number4 > number2 && number4 > number3 && number4 > number5) {
        mayor = number4
    } else if (number5 > number1 && number5 > number2 && number5 > number3 && number5 > number4) {
        mayor = number5
    }
    println("The largest number is: " + mayor)
    println("The smallest number is: " + menor)
}